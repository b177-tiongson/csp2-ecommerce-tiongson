const express = require("express")
const router = express.Router()
const productController = require("../controllers/product")
const auth = require("../auth")


router.post("/",auth.verify,(req,res)=>{

	const data = {
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}


	productController.addProduct(req.body,data)
	.then(resultFromController => res.send(resultFromController))
})

router.get("/",(req,res)=>{

	productController.viewAllProducts()
	.then(resultFromController => res.send(resultFromController))
})

router.get("/:productId",(req,res)=>{

	productController.viewSpecificProduct(req.params)
	.then(resultFromController => res.send(resultFromController))
})

router.put("/:productId",auth.verify,(req,res)=>{

	const data = {
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		name: req.body.name,
		description: req.body.description,
		price: req.body.price,
	}



	productController.editProduct(req.params,data)
	.then(resultFromController => res.send(resultFromController))
})

router.put("/:productId/archive",auth.verify,(req,res)=>{

	const data = {
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
	}

	productController.archiveProduct(req.params,data)
	.then(resultFromController => res.send(resultFromController))
})









module.exports = router