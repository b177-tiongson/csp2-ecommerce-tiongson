//const User = require("../models/User")
const Product = require("../models/Product")
const auth = require("../auth")


//Add New Product
module.exports.addProduct = (reqBody,data)=>{

	if(data.isAdmin){

		let newProduct = new Product({
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price
		})

		return newProduct.save()
		.then((user, error)=>{
			if(error){
				return false
			}
			else{
				return true

			}
		})

	}
	else{
		return Promise.resolve(false)
	}

}

//View all product
module.exports.viewAllProducts = () =>{
	return Product.find({isActive:true})
	.then(result=>{
		return result
	})
}

//view specific product
module.exports.viewSpecificProduct = (reqParams)=>{
	return Product.findById(reqParams.productId)
	.then(result=>{
		return result
	})
}

//edit/update product
module.exports.editProduct = (reqParams, data) =>{

	if(data.isAdmin){

		let updatedProduct = {
			name: data.name,
			description: data.description,
			price: data.price,
		}

		return Product.findByIdAndUpdate(reqParams.productId, updatedProduct)
		.then((product, error)=>{
			if(error){
				return false
			}
			else{
				return true
			}
		})

	}
	else{
		return Promise.resolve(false)
	}

	
}
//archive product
module.exports.archiveProduct = (reqParams,data) =>{

	if(data.isAdmin){

		let archiveProduct = {
			isActive: false
		}

		return Product.findByIdAndUpdate(reqParams.productId, archiveProduct)
		.then((product, error)=>{
			if(error){
				return false
			}
			else{
				return true
			}
		})

	}
	else{
		return Promise.resolve(false)
	}







	
}
