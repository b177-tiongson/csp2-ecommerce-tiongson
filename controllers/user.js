const User = require("../models/User")
const Order = require("../models/Order")
const Product = require("../models/Product")
const bcrypt = require("bcrypt")
const auth = require("../auth")

//User Registration
module.exports.registerUser = (reqBody)=>{

	return User.find({email: reqBody.email})
	.then(result =>{

		console.log(result)

		if(result.length > 0){
			//with duplicate
			return false
		}
		else{
			//without duplicate
			let newUser = new User({
					email: reqBody.email,
					password: bcrypt.hashSync(reqBody.password, 10)
				})

				return newUser.save()
				.then((user, error)=>{
					if(error){
						return false
					}
					else{
						return true

					}
				})
		}

	})

}

//User Login & Generate Access Token
module.exports.loginUser = (reqBody)=>{
	return User.findOne({email:reqBody.email})
	.then(result=>{
		if(result==null){

			return false
		}
		else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect){
				return{access: auth.createAccessToken(result)}
			}
			else{
				return false
			}
		}
	})
}

//Set user as Admin
module.exports.setAsAdmin = (reqParams, data) =>{

	if(data.isAdmin){

		let adminStatus ={isAdmin: true}

		return User.findByIdAndUpdate(reqParams.userId, adminStatus)
		
		.then((user,error)=>{
			if(error){
				return false
			}
			else{
				return true
			}
		})

	}
	else{
		return Promise.resolve(false)

	}

}

//checkout
module.exports.checkout = (reqBody,data) =>{

	if(data.isAdmin==false){

		let a = Object.values(reqBody.order)
		let array = []


		for(let x=0; x<a.length;x++){

			let b = Object.values(a[x])
			let c = b.toString()
			array.push(c)

		}

		return Product.find({_id:{"$in":array}})
		.then(result => {

			if(result == null){
				return false
			}
			else{

				let total = 0
				let prodName = []

				for(let x=0; x<result.length;x++){


					total = total + result[x].price

					prodName.push(result[x].name)


				}

				console.log(prodName)

				let newOrder = new Order({
					userId: data.userId,
					order: reqBody.order,
					totalAmount: total
				})

				
				return newOrder.save()
				.then((user, error)=>{
					if(error){
						return false
					}
					else{
						console.log(newOrder)
						return true

					}
				})	
			}

		})

	}
	else{
		return Promise.resolve(false)
	}
}


//view all orders - admin only
module.exports.viewAllOrder = (data) =>{

	return Order.find({}).then(result =>{

		if(data.isAdmin){
			return result
		}

		else{
			return false
		}

	})
	
}

module.exports.viewMyOrder = (data)=>{

	return Order.find({userId: data.userId}).then(result =>{

		if(data.isAdmin==false){
			return result
		}

		else{
			return false
		}

	})

		

	







}
