const mongoose = require("mongoose")

const orderSchema = new mongoose.Schema({

	totalAmount:{
		type:Number
	},
	userId:{
		type:String,
		require:[true, "User ID is required"]
	},
	purchasedOn:{
		type:Date,
		default: new Date()
	},
	order:[
		{
			productId:{
				type:String,
				require:[true, "Product ID is required"]

			}


		}

	]


})






module.exports = mongoose.model("Order",orderSchema)