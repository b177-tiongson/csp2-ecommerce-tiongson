const mongoose = require("mongoose")

const productSchema = new mongoose.Schema({

	name:{
		type:String,
		required:[true, "Product Name required"]
	},
	description:{
		type:String,
		required:[true, "Product description required"]
	},
	price:{
		type:Number,
		required:[true, "Price required"]
	},
	isActive:{
		type:Boolean,
		default: true
	},
	createdOn:{
		type:Date,
		default: new Date()
	}

})



module.exports = mongoose.model("Product",productSchema)