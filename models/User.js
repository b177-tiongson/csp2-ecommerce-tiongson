const mongoose = require("mongoose")

const userSchema = new mongoose.Schema({

	email:{
		type:String,
		required:[true, "Email required"]
	},
	password:{
		type:String,
		require:[true, "Password required"]
	},
	isAdmin:{
		type:Boolean,
		default:false
	}

})



module.exports = mongoose.model("User",userSchema)